class ApplicationController < ActionController::Base

  include Pundit
  protect_from_forgery with: :exception

before_action :configure_permitted_parameters, if: :devise_controller?

rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private
    def user_not_authorized
      flash[:notice] = "Você não tem permissão para fazer essa ação."
      redirect_to(request.referrer || root_path)
    end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit(:first_name, :last_name, :username, :birthday, :email, :password, :remember_me)
    end
    devise_parameter_sanitizer.permit(:sign_in) do |user_params|
      user_params.permit(:email, :password, :remember_me)
    end
    devise_parameter_sanitizer.permit(:account_update) do |user_params|
      user_params.permit(:first_name, :last_name, :username, :birthday, :about, :email, :password, :current_password, :remember_me)
    end
  end
end
