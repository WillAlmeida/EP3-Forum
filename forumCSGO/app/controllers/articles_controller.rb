class ArticlesController < ApplicationController
  before_action :authenticate_user!
  include ArticlesHelper
  def index
    @articles = Article.all
  end
  def new
    @article = Article.new
  end
  def show
    @article = Article.find(params[:id])
  
  end
  def create
    @article = Article.new(article_params)
    @article.title = params[:article][:title]
    @article.body = params[:article][:body]
    @article.username = current_user.username
    @article.date = Time.now.strftime("%Y/%m/%d")
    @article.save

    redirect_to article_path(@article)
  end
  def edit
    @article = Article.find(params[:id])
  end
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to articles_path
  end
  def update
    @article = Article.find(params[:id])
    @article.update(article_params)
    flash.notice = "Article '#{@article.title}' Updated!"

    redirect_to article_path(@article)
  end
end
