Rails.application.routes.draw do
  get 'control_users/index'

  get 'home/index'
  root "home#index"
  devise_for :users
  resources :articles

  resources :articles do
    resources :comments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
